# BSTTools

A toolchain defined in buildstream. This project is designed to be junctioned to bootstrap your project, and
get out of the way when things are built. The project is intended to be used as a cross compiler.

The list of currently supported architectures and platforms is small at the moment, but will grow over time.

### Supported Build architectures:
* x86-64

### Supported Host architectures:
* x86-64
* aarch32

### Supported Host platforms:
* linux
    * glibc

## Requirements

BSTTools is a buildstream2 project and as such requires a very recent version of buildstream in order to work.

Link: [Master Buildstream Install Instructions.](https://docs.buildstream.build/master/main_install.html)

However, no additional software or modules should be needed besides those mandated by downstream projects.
