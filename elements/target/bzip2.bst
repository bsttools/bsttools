kind: manual
description: |2

  Programs for compressing and decompressing files with a much better compression percentage than with the traditional gzip.

build-depends:
- filename: toolchain.bst

(@): include/environment.yaml

variables:
  makeargs: CC="${CC}" AR="${AR}" RANLIB="${RANLIB}" -j %{max-jobs}

config:
  configure-commands:
  - |
    # ensure installation of symbolic links are relative:
    # ensure the man pages are installed into the correct location:
    # dont run tests, we're cross-compiling
    # honour DESTDIR
    all=$(grep ^all: Makefile | sed "s/test//")
    sed -i \
      -e 's@\(ln -s -f \)$(PREFIX)/bin/@\1@'    \
      -e "s@(PREFIX)/man@(PREFIX)/share/man@g"  \
      -e 's/\$(PREFIX)/\$(DESTDIR)\$(PREFIX)/g' \
      -e "/^all:/c\\${all}"                     \
      -e '/PREFIX=/a\' -e 'DESTDIR='            \
      Makefile

  build-commands:
  - |
    make %{makeargs} -f Makefile-libbz2_so
    make clean
    make %{makeargs}

  install-commands:
  - make DESTDIR=%{install-root} PREFIX=/usr -j 1 install
  - |
    # install the shared bzip2 binary into the /bin directory, make some necessary symbolic links, and clean up:
    mkdir -vp %{install-root}/{bin,lib}
    cp -v bzip2-shared %{install-root}/bin/bzip2
    cp -av libbz2.so* %{install-root}/lib
    ln -sv ../../lib/libbz2.so.1.0 %{install-root}/usr/lib/libbz2.so
    rm -v %{install-root}/usr/bin/{bunzip2,bzcat,bzip2}
    ln -sv bzip2 %{install-root}/bin/bunzip2
    ln -sv bzip2 %{install-root}/bin/bzcat

sources:
- kind: tar
  url: https://www.sourceware.org/pub/bzip2/bzip2-1.0.8.tar.gz
  ref: ab5a03176ee106d3f0fa90e381da478ddae405918153cca248e682cd0c4a2269
