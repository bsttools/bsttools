#!/bin/bash

tools_dir="/tools"

if [[ "" != "$1" ]]; then
  tools_dir="$1"
fi

pushd $(dirname $(readlink -f $0))
for file in gcc/config/{linux,i386/linux{,64}}.h
do
  cp -uv $file{,.orig}
  sed -e "s@/lib\(64\)\?\(32\)\?/ld@${tools_dir}&@g" \
      -e "s@/usr@${tools_dir}@g" $file.orig > $file
  echo "
#undef STANDARD_STARTFILE_PREFIX_1
#undef STANDARD_STARTFILE_PREFIX_2
#define STANDARD_STARTFILE_PREFIX_1 \"${tools_dir}/lib/\"
#define STANDARD_STARTFILE_PREFIX_2 \"\"" >> $file
  touch $file.orig
done
popd
