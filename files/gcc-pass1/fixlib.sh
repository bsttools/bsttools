#!/bin/bash

pushd $(dirname $(readlink -f $0))
case $1 in
  x86-64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig gcc/config/i386/t-linux64
 ;;
esac
popd
