#!/bin/bash

# use debootstrap to create a base layer.

if [ -d stage0 ]; then
	rm -rf stage0
fi
if [ -f stage0.tar.gz ]; then
	rm -rf stage0.tar.gz
fi

mkdir stage0
debootstrap --arch amd64 --include build-essential,texinfo,gawk,bison,python3 stable stage0/ http://deb.debian.org/debian/
rm -rf stage0/dev
rm -f stage0/bin/sh
ln -s /usr/bin/bash stage0/bin/sh
tar czvf stage0.tar.gz -C stage0 .
rm -rf stage0

bst source track stage0/tarball.bst
chmod 777 elements/stage0/tarball.bst

